import 'package:flutter/material.dart';

enum APP_THEME{LIGHT,DARK}
void main() {
  runApp(ContacctProfilePage());
}
class MyAppTheme{
  static ThemeData appThemeLight(){
    return ThemeData(
      brightness: Brightness.light,
      appBarTheme: AppBarTheme(
        color: Colors.purpleAccent,
        iconTheme: IconThemeData(
          // color: Colors.purpleAccent
        ),
      ),
      iconTheme: IconThemeData(
          color: Colors.grey
      ),
    );
  }
  static ThemeData appThemeDark(){
    return ThemeData(
      brightness: Brightness.dark,
      appBarTheme: AppBarTheme(
        color: Colors.purpleAccent,
        iconTheme: IconThemeData(
          // color: Colors.purpleAccent
        ),
      ),
      iconTheme: IconThemeData(
          color: Colors.grey
      ),
    );
  }
}
class ContacctProfilePage extends StatefulWidget{
  @override
  State<ContacctProfilePage> createState() => _ContacctProfilePageState();
}

class _ContacctProfilePageState extends State<ContacctProfilePage> {
  var currentTheme = APP_THEME.LIGHT;
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currentTheme == APP_THEME.DARK ? MyAppTheme.appThemeLight() : MyAppTheme.appThemeDark(),
      home: Scaffold(
        appBar: appBarWidget(),
        body: buildBodyWidget(),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.light),
          onPressed: (){
            setState(() {
              currentTheme == APP_THEME.DARK ? currentTheme = APP_THEME.LIGHT : currentTheme = APP_THEME.DARK;
            });
          },
        ),
      ),
    );
  }
}


Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
         // color: Colors.purpleAccent,
        ),
        onPressed: () {},
      ),
      Text("Call"),
    ],
  );
}
Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.message,
         // color: Colors.purpleAccent,
        ),
        onPressed: () {},
      ),
      Text("Text"),
    ],
  );
}

Widget buildVideoButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.video_call,
        //  color: Colors.purpleAccent,
        ),
        onPressed: () {},
      ),
      Text("Video"),
    ],
  );
}

Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.email,
          //color: Colors.purpleAccent,
        ),
        onPressed: () {},
      ),
      Text("Email"),
    ],
  );
}

Widget buildDirectionsButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.directions,
          // color: Colors.purpleAccent,
        ),
        onPressed: () {},
      ),
      Text("Directions"),
    ],
  );
}

Widget buildPayButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.attach_money,
          // color: Colors.purpleAccent,
        ),
        onPressed: () {},
      ),
      Text("Pay"),
    ],
  );
}
Widget mobilePhoneListTile(){
  return ListTile(
    leading: Icon(Icons.call),
    iconColor: Colors.purpleAccent,
    title: Text("063-249-9382"),
    subtitle: Text("Me"),
    trailing: IconButton(
        icon: Icon(Icons.message),
         color: Colors.purpleAccent,
        onPressed: (){},
    ),
  );
}

Widget otherPhoneListTile(){
  return ListTile(
    leading: Text(" "),
    title: Text("062-829-2425"),
    subtitle: Text("Mom"),
    trailing: IconButton(
      icon: Icon(Icons.message),
       color: Colors.purpleAccent,
      onPressed: (){},
    ),
  );
}

Widget emailListTile(){
  return ListTile(
    leading: Icon(Icons.email_outlined),
    iconColor: Colors.purpleAccent,
    title: Text("s.smilekitdanai@gmail.com"),
    subtitle: Text("work"),
  );
}

Widget addressListTile(){
  return ListTile(
    leading: Icon(Icons.location_on_outlined),
    iconColor: Colors.purpleAccent,
    title: Text(" Bang Saen, Chon Buri, Thailand"),
    subtitle: Text("Home"),
    trailing: IconButton(
  icon: Icon(Icons.cabin_outlined),
   color: Colors.purpleAccent,
  onPressed: (){},
    ),
  );
}

AppBar appBarWidget(){
  return AppBar(
    leading: Icon(Icons.arrow_back,color: Colors.black,
    ),
    actions: <Widget>[
      IconButton(
          icon: Icon(Icons.star_border),
          // color: Colors.black,
          onPressed: (){
            print("Contact is starred");
          }),
    ],
  );

}
Widget buildBodyWidget(){
  return ListView(
    children: <Widget>[
      Column(
        children: <Widget>[
          Container(width: double.infinity,

            //Height constraint at Container widget level
            height: 300,//250
            child: Image.network(
              "https://scontent.fbkk10-1.fna.fbcdn.net/v/t1.6435-9/163304467_1565284193672069_1658584750240804527_n.jpg?_nc_cat=111&ccb=1-7&_nc_sid=09cbfe&_nc_eui2=AeED64r7ilvwwpuGJ-w9VPf-J6NitKY09JEno2K0pjT0kUtjGfIbRxXCJmuTFFC3N6cmkdLs4Cb3IDg0uFEoz5AB&_nc_ohc=wtOVqpBlW90AX8gOmvB&_nc_ht=scontent.fbkk10-1.fna&oh=00_AfAo653FHZdgP0RLWJ0_haIhYOCC0Gbycd3fsYC-fyEiBA&oe=63C9E0FB",
              fit: BoxFit.cover,
            ),
          ),
          Container(
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child :Text("Mild kitdanai",//Priyanke Tyagi
                    style: TextStyle(fontSize: 30),
                  ),
                ),
              ],
            ),
          ),
          Divider(
            color: Colors.grey,
          ),
          Container(
            margin: const EdgeInsets.only(top: 8, bottom: 8),
            child: Theme(
              data: ThemeData(
                iconTheme: IconThemeData(
                  color: Colors.greenAccent,
                ),
              ),
              child: profileActionItem(),
            ),

          ),
          Divider(
            color: Colors.grey,
          ),
          mobilePhoneListTile(),
          otherPhoneListTile(),
          emailListTile(),
          addressListTile(),
        ],
      ),
    ],
  );
}
Widget profileActionItem(){
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        buildCallButton(),
        buildTextButton(),
        buildVideoButton(),
        buildEmailButton(),
        buildDirectionsButton(),
        buildPayButton(),
      ],
  );
}